import os
from PIL import Image
import numpy as np
import matplotlib.pyplot as plt
from sklearn.cluster import KMeans

from classes.UNet.CityscapeDataset import CityscapeDataset
from classes.UNet.UnetModel import UNet

import pandas as pd
import torch
import torch.nn as nn
import torch.nn.functional as F
import torch.optim as optim
from torch.utils.data import Dataset, DataLoader
from torchvision import transforms

from tqdm import tqdm


device = "cuda:0" if torch.cuda.is_available() else "cpu"
device = torch.device(device)

data_dir = os.path.join("data","cityscapes_data")

train_dir = os.path.join(data_dir,"train")
val_dir = os.path.join(data_dir,"val")
train_fns = os.listdir(train_dir)
val_fns = os.listdir(val_dir)

model_name = "U-Net.pth"
sample_image_fp = os.path.join(train_dir,train_fns[0])
sample_image = Image.open(sample_image_fp).convert("RGB")

def split_image(image) :
    image = np.array(image)
    cityscape,label = image[:,:256,:],image[:,256:,:]
    return cityscape,label

sample_image = np.array(sample_image)
cityscape, label = split_image(sample_image)
cityscape,label = Image.fromarray(cityscape),Image.fromarray(label)
fig, axes = plt.subplots(1,2,figsize=(10,5))

"""
color_set = set()
for train_fn in tqdm(train_fns[:10]):
    train_fp = os.path.join(train_dir, train_fn)
    image = np.array(Image.open(train_fp))
    cityscape, label = split_image(sample_image)
    label = label.reshape(-1,3)
    local_color_set = set([tuple(c) for c in list(label)])
    color_set.update(local_color_set)
color_array = np.array(list(color_set))
"""
batch_size = 16
epochs = 10
lr = 0.01
num_classes = 10
num_items = 1000

color_array = np.random.choice(range(256),3*num_items).reshape(-1,3)

label_model = KMeans(n_clusters=num_classes)
label_model.fit(color_array)
label_model.predict(color_array[:5,:])

cityscape, label = split_image(sample_image)
label_class = label_model.predict(label.reshape(-1,3)).reshape(256,256)
fig, axes = plt.subplots(1,3,figsize=(15,5))

dataset = CityscapeDataset(train_dir,label_model)
plt.savefig("imgs/model exp.png")
model = UNet(num_classes=num_classes).to(device)
data_loader = DataLoader(dataset,batch_size=batch_size)

criterion = nn.CrossEntropyLoss()
optimizer = optim.Adam(model.parameters(),lr=lr)
model.load_state_dict(torch.load(model_name))
step_losses = []
epoch_losses = []
for epoch in tqdm(range(epochs)):
    epoch_loss = 0
    for X, Y in tqdm(data_loader,total=len(data_loader),leave=False):
        X, Y = X.to(device), Y.to(device)
        optimizer.zero_grad()
        Y_pred = model(X)
        loss = criterion(Y_pred,Y)
        loss.backward()
        optimizer.step()
        epoch_loss+= loss.item()
        step_losses.append(loss.item())
    epoch_losses.append(epoch_loss/len(data_loader))


fig,axes = plt.subplots(1,2,figsize = (10,5))
axes[0].plot(step_losses)
axes[1].plot(epoch_losses)
plt.savefig('imgs/model predict.png')

torch.save(model.state_dict(),model_name)
