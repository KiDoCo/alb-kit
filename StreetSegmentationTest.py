import torch
from tqdm import tqdm
import numpy as np
import os
import matplotlib.pyplot as plt
from classes.UNet.CityscapeDataset import CityscapeDataset 
from classes.UNet.UnetModel import UNet
from torch.utils.data import Dataset, DataLoader
from torchvision import transforms
from classes.UNet.public_var_class import public_var_class
from sklearn.cluster import KMeans
from PIL import Image

num_classes = 10
test_batch_size = 8
model_path = "U-Net.pth"
device = "cuda:0" if torch.cuda.is_available() else "cpu"
device = torch.device(device)

data_dir = os.path.join("data","cityscapes_data")
val_dir = os.path.join(data_dir,"val")
val_fns = os.listdir(val_dir)
variable_class = public_var_class()

sample_image_fp = os.path.join(val_dir,val_fns[0])
sample_image = Image.open(sample_image_fp).convert("RGB")
sample_image = np.array(sample_image)
cityscape,label = variable_class.split_image(sample_image)
cityscape,label = Image.fromarray(cityscape),Image.fromarray(label)
fig,axes = plt.subplots(1,2,figsize=(10,5))

label_model = KMeans(n_clusters=variable_class.num_classes)
label_model.fit(variable_class.color_array)
label_model.predict(variable_class.color_array[:5,:]) 
dataset = CityscapeDataset(val_dir,label_model)

model = UNet(num_classes=num_classes).to(variable_class.device_pick)
model.load_state_dict(torch.load(model_path))
data_loader = DataLoader(dataset,batch_size=test_batch_size)

X,Y = next(iter(data_loader))
X,Y = X.to(variable_class.device_pick),Y.to(variable_class.device_pick)
Y_pred = model(X)
print(Y_pred.shape)
Y_pred = torch.argmax(Y_pred,dim=1)
print(Y_pred.shape)

inverse_transform = transforms.Compose([transforms.Normalize((-0.485/0.229,-0.456/0.224,-0.406/0.225),(1/0.229,1/0.224,1/0.225))])

fig,axes = plt.subplots(test_batch_size,3,figsize=(3*5,test_batch_size*5))

for i in tqdm(range(test_batch_size)):

    landscape = inverse_transform(X[i]).permute(1,2,0).cpu().detach().numpy()
    
    label_class = Y[i].cpu().detach().numpy()
    label_class_predicted = Y_pred[i].cpu().detach().numpy()

    axes[i,0].imshow(landscape)
    axes[i,0].set_title("Landscape")
    axes[i,1].imshow(label_class)
    axes[i,1].set_title("Label Class")
    axes[i,2].imshow(label_class_predicted)
    axes[i,2].set_title("Label CLass - predicted")

plt.savefig("imgs/model_result")
