Requires
- matplotlib
- NumPy
- Pytorch (school comp, CUDA 10.0 version latest)
- OpenCV
- Python 3.+