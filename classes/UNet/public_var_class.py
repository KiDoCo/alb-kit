import os
import numpy as np
import torch
from tqdm import tqdm



class public_var_class():

    def __init__(self):

         """
         color_set = set()
         for train_fn in tqdm(train_fns[:10]):
            train_fp = os.path.join(train_dir, train_fn)
            image = np.array(Image.open(train_fp))
            cityscape, label = split_image(sample_image)
            label = label.reshape(-1,3)
            local_color_set = set([tuple(c) for c in list(label)])
            color_set.update(local_color_set)
          color_array = np.array(list(color_set))
          """
          
         self.num_items = 1000
         self.num_classes = 10
         self.lr = 0.01
         self.color_array = np.random.choice(range(256),3*self.num_items).reshape(-1,3)
         self.device_pick = "cuda:0" if torch.cuda.is_available() else "cpu"
         self.device_pick = torch.device(self.device_pick)
         self.batch_size = 16
         self.epochs = 10

    def split_image(self,image):
        image = np.array(image)
        cityscape,label = image[:,:256,:],image[:,256:,:]
        return cityscape,label
